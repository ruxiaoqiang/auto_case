import allure
import pytest
import os


class Test_001:
    # 函数的开始
    def setup(self):
        print('rxq11111111111')

    # @pytest.mark.finished
    def test_cc(self):  # test开头的测试函数
        # 遇到false即为失败
        assert True, '111'
        for i in range(5):
            print(5)
        # assert False, '2222'

    # @pytest.mark.unfinished
    def test_aa(self):  # test开头的测试函数
        assert True, '222'

    @pytest.mark.skip(reason='out-of-date api')
    def test_aa1(self, okkk):  # test开头的测试函数
        assert okkk == 2, '222'

    # 预期失败，若成功则是xpass
    @pytest.mark.xfail(reason='not supported until v0.2.0')
    def test_raises(self):
        with pytest.raises(ValueError) as e:
            pass
        exec_msg = e.value.args[0]
        assert exec_msg == "invalid literal for int() with base 10: 'a'"

    # 函数的结束
    def teardown(self):
        print('rxq22222222')

    # 多一个参数就多跑一次
   # @pytest.mark.parametrize('password', ['123', '123', '123'],'asd',['123','2323','12312412'])
   # def test_yyy(self, password):
       # print(password)
       # assert password == '123'
